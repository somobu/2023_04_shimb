class_name Loco
extends CharacterBody3D



@onready var root: Root = get_node("/root/Root")

var current_location: Location = null
var current_path: Path3D = null

@export var move_speed: float = 12.0 # Units/s
@export var is_stopped = true
var current_speed = 0.0


func _input(event):
	if event is InputEventMouseMotion:
#		$CamBase.rotation_degrees.y += 0.3 * event.relative.x
		pass


func _process(delta):
	if is_stopped:
		current_speed -= 6.0 * delta
		if current_speed < 0: current_speed = 0
	else:
		current_speed += 1.0 * delta
		if current_speed > move_speed: current_speed = move_speed
	
	global_position.x += current_speed * delta



func on_pivot_activated(target: Node3D):
	if not root.is_in_blocking_dialog:
		$CamBase/Camera3D.fov = target.fov
		$CamBase.global_transform = target.global_transform
