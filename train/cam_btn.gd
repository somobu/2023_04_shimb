@tool
extends TextureButton

signal activated(target: Node3D)


@export var active = false

@export_node_path("Node3D") var target = null

@export var active_normal: Texture2D
@export var active_hover: Texture2D
@export var inactive_normal: Texture2D
@export var inactive_hover: Texture2D


func _ready():
	active = false
	update_tex()
	
	$Label.text = name
	$Label.hide()


func _pressed():
#	active = !active
	update_tex()
	
	if target != null:
		emit_signal("activated", get_node(target))


func update_tex():
	if active:
		texture_normal = active_normal
		texture_hover = active_hover
	else:
		texture_normal = inactive_normal
		texture_hover = inactive_hover


func _on_mouse_entered():
	$Label.show()


func _on_mouse_exited():
	$Label.hide()
