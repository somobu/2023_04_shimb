class_name Location
extends Node3D

# A = affection, R = height
var heightmap: Image = null
var heightmap_file = "user:///gen/rails50_hm.png"

# A = affection, R = gravel
var texmap: Image = null
var texmap_file = "user://gen/rails50_tx.png"


func get_size() -> Vector2i:
	return Vector2i(50, 28)


func gen_textures():
	if not FileAccess.file_exists(heightmap_file):
		heightmap = gen_heightmap()
		heightmap.save_png(heightmap_file)
	else:
		heightmap = Image.load_from_file(heightmap_file)
	
	if not FileAccess.file_exists(texmap_file):
		texmap = gen_texmap()
		texmap.save_png(texmap_file)
	else:
		texmap = Image.load_from_file(texmap_file)


func get_slice(val_max = 1.5, fac = 1.5):
	var slice = []
	slice.resize(get_size().y)
	
	var halfsize = get_size().y / 2.0
	for i in range(get_size().y):
		var y = (i - halfsize) - 1
		var norm_y = y / halfsize
		slice[i] = val_max - fac * norm_y * norm_y
		if slice[i] > 1: slice[i] = 1
		if slice[i] < 0: slice[i] = 0
	
	return slice


func gen_heightmap():
	var map = Image.create(get_size().x, get_size().y, false, Image.FORMAT_RGBA8)
	var slice = get_slice()
	
	for x in range(get_size().x):
		for y in range(get_size().y):
			var val = slice[y]
			var affection = val
			var height = 0.5
			map.set_pixel(x, y, Color(height, 0, 0, affection))
	
	return map


func gen_texmap():
	var map = Image.create(get_size().x, get_size().y, false, Image.FORMAT_RGBA8)
	var slice = get_slice(1.0, 1.5)
	
	for x in range(get_size().x):
		for y in range(get_size().y):
			var affection = slice[y]
			map.set_pixel(x, y, Color(1.0, 0, 0, affection))
	
	return map
