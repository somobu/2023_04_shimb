class_name ChunkMgr
extends Node3D

signal worker_step


const chunk_res = preload("res://chunks/chunk.tscn")
const chunk_side = 500.0


var worker_thread = Thread.new()
var worker_thread_notify = Semaphore.new()

var queue = []
var queue_lock = Mutex.new()

var node_follow = null
var current_bucket = -1


func _init():
	assert(not worker_thread.is_started())
	worker_thread.start(_worker_thread)


func _physics_process(_delta):
	var buckets_dist = 1 # central + 1
	var total_buckets = 1 + 2 * buckets_dist
	
	if node_follow != null:
		var map = []
		for i in range(total_buckets): map.append(false)
		
		var target_bucket = get_bucket_glob(node_follow)
		if current_bucket == target_bucket: return
		
		current_bucket = target_bucket
		
		for child in get_children():
			if not child.visible: continue
			
			var child_bucket = get_bucket(child)
			var rel_bucket = child_bucket - target_bucket + 1
			
			if rel_bucket < 0 or rel_bucket >= total_buckets: 
				child.visible = false
				remove_child(child)
				child.start_dest_thr()
			else: 
				map[rel_bucket] = true
		
		for idx in range(total_buckets):
			if not map[idx]:
				var glob_idx = current_bucket + idx - buckets_dist
				var pos = Vector3(chunk_side * glob_idx, 0, 0)
				queue_add(pos)


func get_bucket(node: Node3D) -> int:
	return floor((node.position.x + chunk_side/2) / chunk_side)


func get_bucket_glob(node: Node3D) -> int:
	return floor((node.global_position.x + chunk_side/2) / chunk_side)


func _worker_thread():
	while true:
		worker_thread_notify.wait()
		
		queue_lock.lock()
		var own_queue = queue.duplicate()
		queue_lock.unlock()
		
		for q_pos in own_queue:
			var chunk: Chunk = chunk_res.instantiate()
			chunk.position = q_pos
			chunk.name = "Chunk_%d" % get_bucket(chunk)
			chunk.gen_terrain(q_pos)
			add_child.call_deferred(chunk)
		
		queue_lock.lock()
		for q_pos in own_queue:
			queue.erase(q_pos)
		queue_lock.unlock()


func queue_add(q_pos: Vector3):
	queue_lock.lock()
	if not queue.has(q_pos):
		queue.append(q_pos)
	queue_lock.unlock()
	
	worker_thread_notify.post()

