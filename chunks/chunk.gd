class_name Chunk
extends Node3D

const spruce_ref = preload("res://spruce.tscn")

var noise = FastNoiseLite.new()

func _ready():
	noise.seed = 4221


func start_dest_thr():
	# How do I destroy chunk?
	push_error("Memory leak happens right here")


func get_width() -> float:
	return 500.0

func get_height() -> float:
	return 500.0


func gen_terrain(pos: Vector3):
	print("gt - start")
	
	var height_fac = 10.0;
	
	# Texture props
	var maps_size = Vector2(500, 500) # In pixels
	
	var terrain_mesh = $Terrain/Mesh
	var locations = $Locations
	var trees = $Trees
	
	var s = Time.get_ticks_msec()
	
	var ch_pos = Vector2(pos.x, pos.z)
	var half_chunk = Vector2(get_width() / 2.0, get_height() / 2.0)
	
	# Generate output maps
	var tex_himap = Image.create(maps_size.x, maps_size.y, false, Image.FORMAT_RGBA8)
	var tex_texmap = Image.create(maps_size.x, maps_size.y, false, Image.FORMAT_RGBA8)
	for x in range(maps_size.x):
		for y in range(maps_size.y):
			var local_pos = Vector2(x - maps_size.x, y - maps_size.y) - half_chunk
			var glob_pos = local_pos + ch_pos
			var noise_height = noise.get_noise_2d(glob_pos.x, glob_pos.y) + 0.5
			tex_himap.set_pixel(x, y, Color(noise_height, noise_height, noise_height))
			tex_texmap.set_pixel(x, y, Color.BLACK)
	
	
	# Spawn locations
	for child in locations.get_children():
		if child is LocationSpawner:
			var spawner = child as LocationSpawner
			var loc = spawner.get_scene().instantiate() as Location
			loc.name = spawner.name
			loc.position = spawner.position
			
			locations.remove_child(spawner)
			locations.add_child(loc)
			spawner.queue_free()
			
			loc.gen_textures()
			
			var loc_size = Vector2(loc.get_size())
			var loc_pos = Vector2(loc.position.x, loc.position.z) - loc_size / 2
			var offset = loc_pos + Vector2(get_width(), get_height()) / 2
			
			for x in range(loc.get_size().x):
				for y in range(loc.get_size().y):
					var glob_px = Vector2(x + offset.x, y + offset.y)
					
					var height_base = tex_himap.get_pixelv(glob_px)
					var height_loc = loc.heightmap.get_pixelv(Vector2(x,y))
					var height = lerp(height_base.r, height_loc.r, height_loc.a)
					tex_himap.set_pixelv(glob_px, Color(height, height, height))
					
					var tex_base = tex_texmap.get_pixelv(glob_px)
					var tex_loc = loc.texmap.get_pixelv(Vector2(x,y))
					var tex = lerp(tex_base.r, tex_loc.r, tex_loc.a)
					tex_texmap.set_pixelv(glob_px, Color(tex, tex_base.g, tex_base.b))
	
	
	# Assign maps and stuff
	var mat = terrain_mesh.mesh.surface_get_material(0).duplicate()
	mat.set_shader_parameter("texmap", ImageTexture.create_from_image(tex_texmap))
	mat.set_shader_parameter("heightmap", ImageTexture.create_from_image(tex_himap))
	mat.set_shader_parameter("scale_fac", Vector3(get_width() / 4.0, get_height() / 4.0, height_fac))
	terrain_mesh.set_surface_override_material(0, mat)
	
	
	# Spawn trees
	var points = get_foliage_points()
	
	for point_idx in range(points.size()):
		var point = points[point_idx]
		var x = point.x
		var y = point.y
		
		var spruce = spruce_ref.instantiate()
		var moss_channel = tex_texmap.get_pixel(x,y).r
		if moss_channel > 0.3: continue
		
		var height_total = tex_himap.get_pixel(x,y).r * height_fac
		
		var local_pos = Vector3(x, height_total, y)
		local_pos -= Vector3(get_width(), 0, get_height())/2
	
		trees.add_child(spruce)
		spruce.position = local_pos

	var e = Time.get_ticks_msec()
	print("gt - end in ", e-s, " ms")


func get_foliage_points():
	var points = []
	
	var circle_step = 9.0
	
	var rect = Vector2(get_width(), get_height())
	var smaller_side = rect.x if rect.x < rect.y else rect.y
	var bigger_side = rect.x if rect.x > rect.y else rect.y
	
	var center_a = Vector2(-0.5,+1.5) * rect
	var center_b = Vector2(-0.5,-0.5) * rect
	
	var d = (center_a - center_b).length()
	
	for r_a in range(0.5 * smaller_side, 2 * bigger_side, circle_step):
		for r_b in range(0.5 * smaller_side, 2 * bigger_side, circle_step):
			if r_a + r_b < d: continue
			
			var a = (r_a*r_a - r_b*r_b + d*d) / (2*d)
			var h = sqrt(r_a*r_a - a*a)
			
			var p2 = center_a + a * (center_b - center_a) / d
			
			var i1 = Vector2(
				p2.x + h * (center_a.y - center_b.y) / d,
				p2.y - h * (center_a.x - center_b.x) / d
			)
			
			var i2 = Vector2(
				p2.x - h * (center_a.y - center_b.y) / d,
				p2.y + h * (center_a.x - center_b.x) / d
			)
			
			var di1 = i1 + get_jitter(i1) * circle_step / 4
			if di1.x >= 0 and di1.x < rect.x and di1.y >= 0 and di1.y < rect.y:
				points.push_back(di1)
				
			if i1.is_equal_approx(i2): continue
			
			var di2 = i2 + get_jitter(i2) * circle_step / 4
			if di2.x >= 0 and di2.x < rect.x and di2.y >= 0 and di2.y < rect.y:
				points.push_back(di2)
			
	return points


func get_jitter(_local_pos: Vector2) -> Vector2:
	return Vector2(randf_range(-1,1), randf_range(-1,1))
