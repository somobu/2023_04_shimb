class_name LocationSpawner
extends Marker3D

@export_enum("railroad", "B", "C") var spawn_list: int;
@export var size_hint = Vector2(50, 50);

func get_scene() -> PackedScene:
	match spawn_list:
		0: return load("res://locations/rails_50.tscn")
	
	return null
