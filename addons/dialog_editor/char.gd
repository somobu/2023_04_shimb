@tool
extends Control

var avatar_res = null

func set_state(state: Dictionary):
	$CharId.text = name
	$CharName.text = state["name"]
	
	avatar_res = state["avatar"]
	$Portrait/TextureRect.texture = load(avatar_res)


func get_overriden_id() -> String:
	return $CharId.text


func get_state() -> Dictionary:
	return {
		"name": $CharName.text,
		"avatar": avatar_res
	}
