@tool
extends EditorPlugin

var main_panel_inst: Control

func _enter_tree():
	var packed = load("res://addons/dialog_editor/dialog_editor_panel.tscn")
	main_panel_inst = packed.instantiate()
	main_panel_inst.plugin_ref = self
	get_editor_interface().get_editor_main_screen().add_child(main_panel_inst)
	
	_make_visible(false)


func _exit_tree():
	if main_panel_inst:
		main_panel_inst.queue_free()


func _has_main_screen():
	return true


func _make_visible(visible):
	if main_panel_inst:
		main_panel_inst.visible = visible


func _get_plugin_name():
	return "Dialogs"


func _get_plugin_icon():
	return get_editor_interface().get_base_control().get_theme_icon("Node", "EditorIcons")
