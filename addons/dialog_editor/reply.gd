@tool
extends HBoxContainer

signal remove_requested(my_name: String)

const color_none = Color(1,1,1)
const color_active = Color(1, 0.75, 0.52)

var icon = null



func set_state(state: Dictionary):
	var text = state["text"]
	set_text(text)
	
	var icon = state["icon"] if "icon" in state else null
	set_icon(icon)
	
	$PopupFilter/VBoxContainer/FlagsWhite.text = ",".join(state["filter_flags_white"] if "filter_flags_white" in state else [])
	$PopupFilter/VBoxContainer/FlagsBlack.text = ",".join(state["filter_flags_black"] if "filter_flags_black" in state else [])
	$PopupFlags/VBoxContainer/Set.text = ",".join(state["flags_set"] if "flags_set" in state else [])
	$PopupFlags/VBoxContainer/Unset.text = ",".join(state["flags_unset"] if "flags_unset" in state else [])
	
	$PopupFilter/VBoxContainer/FilterFcn.text = state["filter_fcn"] if "filter_fcn" in state else ""
	$PopupActions/VBoxContainer/OnchooseFcn.text = state["fcn"] if "fcn" in state else ""
	
	popup_filters_hide()
	popup_flags_hide()
	popup_actions_hide()
	


func get_state() -> Dictionary:
	var state = {
		"text": get_text()
	}
	
	var icon = get_icon()
	if icon != null: state["icon"] = icon
	
	var fcn: String = $PopupActions/VBoxContainer/OnchooseFcn.text
	fcn = fcn.strip_edges()
	if not fcn.is_empty(): state["fcn"] = fcn
	
	var filter_fcn: String = $PopupFilter/VBoxContainer/FilterFcn.text
	filter_fcn = filter_fcn.strip_edges()
	if not filter_fcn.is_empty(): state["filter_fcn"] = filter_fcn
	
	var filter_flags_white = get_filter_flags_white()
	if filter_flags_white.size() > 0:
		state["filter_flags_white"] = filter_flags_white
	
	var filter_flags_black = get_filter_flags_black()
	if filter_flags_black.size() > 0:
		state["filter_flags_black"] = filter_flags_black
	
	var flags_set = get_flags_set()
	if flags_set.size() > 0:
		state["flags_set"] = flags_set
	
	var flags_unset = get_flags_unset()
	if flags_unset.size() > 0:
		state["flags_unset"] = flags_unset
	
	return state


func on_remove_btn_click():
	emit_signal("remove_requested", name)


func set_text(text):
	$LineEdit.text = text

func get_text():
	return $LineEdit.text


func set_icon(res):
	icon = res
	
	if res != null:
		$BtnIcon/TextureRect.texture = load(res)
	else:
		$BtnIcon/TextureRect.texture = null

func get_icon():
	return icon


func get_filter_flags_white() -> Array:
	var text: String = $PopupFilter/VBoxContainer/FlagsWhite.text
	return text.split(",", false)

func get_filter_flags_black() -> Array:
	var text: String = $PopupFilter/VBoxContainer/FlagsBlack.text
	return text.split(",", false)

func get_flags_set() -> Array:
	var text: String = $PopupFlags/VBoxContainer/Set.text
	return text.split(",", false)

func get_flags_unset() -> Array:
	var text: String = $PopupFlags/VBoxContainer/Unset.text
	return text.split(",", false)


func show_filters_popup():
	$PopupFilter.show()

func popup_filters_hide():
	var filter_fcn_raw: String = $PopupFilter/VBoxContainer/FilterFcn.text.strip_edges()
	var fcn = not filter_fcn_raw.is_empty()
	var white = get_filter_flags_white().size() > 0
	var black = get_filter_flags_black().size() > 0
	
	var changed = fcn or white or black
	$BtnFilter/TextureRect.modulate = color_active if changed else color_none
	
	$PopupFilter.hide()

func popup_flags_show():
	$PopupFlags.show()

func popup_flags_hide():
	var set = get_flags_set().size() > 0
	var unset = get_flags_unset().size() > 0
	
	var changed = set or unset
	$BtnFlag/TextureRect.modulate = color_active if changed else color_none
	
	$PopupFlags.hide()

func popup_actions_show():
	$PopupActions.show()

func popup_actions_hide():
	var fcn_raw: String = $PopupActions/VBoxContainer/OnchooseFcn.text.strip_edges()
	$BtnActions/TextureRect.modulate = color_none if fcn_raw.is_empty() else color_active
	$PopupActions.hide()
