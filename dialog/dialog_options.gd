class_name DialogOptions
extends Control

signal option_selected(idx: int)

var is_blocking = false

var pos_min = Vector2(776, 480)
var pos_max = Vector2(720, 424)

var current_ids = []


func _input(event):
	if not visible: return
	
	var interact_option_dir = 0
	if event.is_action_released("interact_options_up"):		interact_option_dir += -1
	if event.is_action_released("interact_options_down"): 	interact_option_dir += +1
	
	var selectedList = $Options.get_selected_items()
	var selectedItem = selectedList[0] if selectedList.size() > 0 else 0
	
	if interact_option_dir != 0:
		selectedItem += interact_option_dir
		
		if selectedItem < 0: 
			selectedItem = 0
		if selectedItem >= $Options.item_count: 
			selectedItem = ($Options.item_count - 1)
		
		$Options.select(selectedItem)
	
	if event.is_action_released("interact"):
		emit_signal("option_selected", current_ids[selectedItem])


func offer_new_entries(entries, icons = []):
	var can_switch_entries = not visible or (visible and not is_blocking)
	if not can_switch_entries: return
	
	set_new_entries(entries, icons)


func set_new_entries(entries = [], ids = [], icons = []):
	$Options.clear()
	current_ids = ids
	
	for entry in entries:
		$Options.add_item(entry)
	
	for i in range(icons.size()):
		if icons[i] == null: continue
		$Options.set_item_icon(i, load(icons[i]))
	
	for i in range($Options.item_count):
		$Options.set_item_tooltip_enabled(i, false)
	
	$Options.select(0)
