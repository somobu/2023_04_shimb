class_name DialogMgr
extends Control

signal dialog_enter
signal dialog_over

const ENTRY_POINT_NAME = "entry"

var current_dialog_file = null

var dialog_data = null
var dialog_entry_id = null
var is_dialog_entered = false

var state = {
	"flags": []
}


func on_option_selected(index):
	var entry = dialog_data["dialog"][dialog_entry_id]
	var resp = entry["opts"][index]
	
	if "flags_unset" in resp:
		for flag in resp["flags_unset"]:
			state["flags"].erase(flag)
	
	if "flags_set" in resp:
		state["flags"].append_array(resp["flags_set"])
	
	if "fcn" in resp:
		call(resp["fcn"])
	
	if not "to" in resp:
		emit_signal("dialog_over")
		hide()
		return
	
	switch_dialog_entry(resp["to"])


func show_dialog(dialog_filename: String):
	current_dialog_file = dialog_filename
	
	var f = FileAccess.open(dialog_filename, FileAccess.READ)
	dialog_data = JSON.parse_string(f.get_as_text())
	
	switch_dialog_entry(ENTRY_POINT_NAME)


func switch_dialog_entry(entry_name: String):
	if entry_name == ENTRY_POINT_NAME:
		is_dialog_entered = false
	elif not is_dialog_entered:
		is_dialog_entered = true
		emit_signal("dialog_enter")
	
	dialog_entry_id = entry_name
	
	var entry = dialog_data["dialog"][entry_name]
	var chara = dialog_data["chars"][entry["char"]]
	
	# Process dialog line
	var mode = "rand" if not "text_mode" in entry else entry["text_mode"]
	if mode == ENTRY_POINT_NAME:
		hide_dialog_line()
	else:
		set_dialog_line(chara["name"], entry["text"])
	
	
	# Build answer options
	var options = []
	var ids = []
	var icons = []
	
	for opt in entry["opts"]:
		
		# Flags black/white-lists
		if "filter_flags_black" in opt:
			var should_skip = false
			for flag in opt["filter_flags_black"]:
				if flag in state["flags"]:
					should_skip = true
			if should_skip: continue
		
		if "filter_flags_white" in opt:
			var should_skip = false
			for flag in opt["filter_flags_white"]:
				if not flag in state["flags"]:
					should_skip = true
			if should_skip: continue
		
		# Perform custom filtering
		if "filter_fcn" in opt and call(opt["filter_fcn"]) == false:
			continue
		
		options.append(opt["text"])
		ids.append(entry["opts"].find(opt))
		
		if "icon" in opt: icons.append(opt["icon"])
		elif not "to" in opt: icons.append("res://dialog/ic_leave.svg")
		else: icons.append(null)
	
	set_dialog_options(options, ids, icons)


func hide_dialog_line():
	for node in get_children():
		if node is DialogEntry:
			node.hide()


func set_dialog_line(char_name: String, text: String):
	for node in get_children():
		if node is DialogEntry:
			(node as DialogEntry).set_dialog_line(char_name, text)


func set_dialog_options(options: Array, ids: Array, icons: Array):
	for node in get_children():
		if node is DialogOptions:
			node.set_new_entries(options, ids, icons)




# Content-related stuff

@onready var root: Root = get_node("/root/Root")
@onready var train: Loco = get_node("/root/Root/Train")

func stop_train():
	train.is_stopped = true

func start_train():
	train.is_stopped = false

func train_running():
	return not train.is_stopped

func train_stopped():
	return train.is_stopped
