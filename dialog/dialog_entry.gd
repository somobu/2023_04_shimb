class_name DialogEntry
extends Control

const visible_char_cooldown = 0.025

var current_vis_char_cooldown = 0


func _ready():
	$TextLog/Label.text = "What took you so long?"


func _physics_process(delta):
	current_vis_char_cooldown -= delta
	
	if current_vis_char_cooldown < 0:
		current_vis_char_cooldown = visible_char_cooldown
		if $TextLog/Label.visible_characters < $TextLog/Label.text.length():
			$TextLog/Label.visible_characters += 1


func reset_line():
	$Person/Label.text = ""
	$TextLog/Label.text = ""


func set_dialog_line(author: String, message: String):
	$Person/Label.text = author
	$TextLog/Label.text = message
	$TextLog/Label.visible_characters = 1
	show()

