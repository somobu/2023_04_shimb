extends Node3D


@onready var root: Root = get_node("/root/Root")


func get_interaction_type() -> String:
	return Root.INTERACTION_DIALOG

func get_dialog_filename() -> String:
	return "res://chars/roenji/dialog_roenji.json"

func is_dialog_blocking() -> bool:
	return true



func _on_mouse_entered():
	$Label3D.show()
	root.mouse_interaction_enter(self)


func _on_mouse_exited():
	$Label3D.hide()
	root.mouse_interaction_exit(self)
