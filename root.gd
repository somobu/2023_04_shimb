class_name Root
extends Node3D

const INTERACTION_DIALOG = "dialog"

const radio_dialog_file = "res://chars/roenji/dialog_radio.json"

var current_interacted_node = null

var is_in_radio_dialog = false
var is_dialog_entered = false
var is_in_blocking_dialog = false


@onready var chunk_mgr = get_node("/root/Root/ChunkMgr") as ChunkMgr
@onready var dialog_mgr = get_node("/root/Root/DialogMgr") as DialogMgr


func _ready():
	if true:
		var dir_udata = OS.get_user_data_dir()
		var dir_gen = dir_udata + "/gen"
		print("Userdata is \"", dir_udata, "\"")
		print("Removing generated maps...")
		OS.execute("rm", ["-rf", dir_gen])
		DirAccess.make_dir_absolute("user://gen/")
	
	var initial_chunk: Chunk = get_node("ChunkMgr/Chunk")
	initial_chunk.gen_terrain(initial_chunk.global_position)
	
	chunk_mgr.node_follow = $Train


func _input(event):
	if event.is_action("radio"):
		if is_in_blocking_dialog: return
		if is_dialog_entered: return
		
		if event.is_pressed() and not dialog_mgr.visible:
			dialog_mgr.show_dialog(radio_dialog_file)
			dialog_mgr.show()
			is_in_radio_dialog = true
		elif not event.is_pressed() and is_in_radio_dialog:
			dialog_mgr.hide()
			is_in_radio_dialog = false



func mouse_interaction_enter(node: Node):
	if is_in_blocking_dialog: return
	
	current_interacted_node = node
	
	if node.has_method("get_interaction_type"):
		var int_type: String = node.get_interaction_type()
		if int_type == INTERACTION_DIALOG and not dialog_mgr.visible:
			dialog_mgr.show_dialog(node.get_dialog_filename())
			dialog_mgr.show()


func mouse_interaction_exit(node: Node):
	
	if node == current_interacted_node:
		current_interacted_node = null
		
		if not is_in_blocking_dialog and not is_in_radio_dialog:
			$DialogMgr.hide()


func on_dialog_enter():
	is_dialog_entered = true
	if current_interacted_node == null: return
	if not current_interacted_node.has_method("is_dialog_blocking"): return
	
	is_in_blocking_dialog = current_interacted_node.is_dialog_blocking()


func on_dialog_over():
	is_dialog_entered = false
	is_in_blocking_dialog = false
